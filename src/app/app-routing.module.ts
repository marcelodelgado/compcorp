import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogPageComponent } from './module/log/log-page/log-page.component';
import { DashboardPageComponent } from './module/dashboard/dashboard-page/dashboard-page.component';
import { HowToComponent } from './module/how-to/how-to.component';
import { ConfigPageComponent } from './module/config/config-page/config-page.component';

const routes: Routes = [
  {path:"", component:HowToComponent},
  {path:"log", component:LogPageComponent},  
  {path:"dashboard", component:DashboardPageComponent},  
  {path:"config", component:ConfigPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
