import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

//TODO FAZER IF EM TODAS AS PAGINAS CHECANDO SE EXISTE USUÁRIO LOGADO E SE NÃO MANDAR O USUARIO SELECIONAR ALGUM USUÁRIO NA ABA CONFIG

export class AppComponent {  
  title = 'CompCorp';
  items: Observable<any[]>;
  
  constructor() {
  }
}


