import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//Main
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//Shared
import { NavComponent } from './shared/component/nav/nav.component';
import { FooterComponent } from './shared/component/footer/footer.component';
//Module
import { HowToComponent } from './module/how-to/how-to.component';
import { ConfigPageComponent } from './module/config/config-page/config-page.component';
import { ConfigTitleComponent } from './module/config/config-title/config-title.component';
import { ConfigFormComponent } from './module/config/config-form/config-form.component';
import { ConfigUserListComponent } from './module/config/config-user-list/config-user-list.component';
import { LogPageComponent } from './module/log/log-page/log-page.component';
import { LogFormComponent } from './module/log/log-form/log-form.component';
import { LogDataComponent } from './module/log/log-data/log-data.component';
import { DashboardPageComponent } from './module/dashboard/dashboard-page/dashboard-page.component';
import { DashboardGraphsComponent } from './module/dashboard/dashboard-graphs/dashboard-graphs.component';
//Node
import { AlertModule } from 'ngx-bootstrap';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { DashboardAlertComponent } from './module/dashboard/dashboard-alert/dashboard-alert.component';
import { DashboardObjectivesComponent } from './module/dashboard/dashboard-objectives/dashboard-objectives.component';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    HowToComponent,
    ConfigPageComponent,
    ConfigTitleComponent,
    ConfigFormComponent,
    ConfigUserListComponent,
    LogPageComponent,
    LogFormComponent,
    LogDataComponent,
    DashboardPageComponent,
    DashboardGraphsComponent,
    DashboardAlertComponent,
    DashboardObjectivesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    FormsModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
