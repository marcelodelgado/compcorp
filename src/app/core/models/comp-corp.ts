export class CompCorp {
    userKey : string = "";
    data : string = "";
    dataObjeto : Date;
    peso : Number = null;
    pesoDelta : Number = null;
    pesoAcumulado : Number = null;
    pesoEstimado : Number = null;
    gordura : Number = null;
    gorduraKg : Number = null;
    gorduraEstimada : Number = null;
    gorduraDelta : Number = null;
    gorduraAcumulada : Number = null;
    hidratacao : Number = null;
    massaMagraKg : Number = null;
    massaMagraDelta : Number = null;
    massaMagraAcumulado : Number = null;
    musculo : Number = null;
    musculoKg : Number = null;
    musculoEstimado : Number = null;
    musculoDelta : Number = null;
    musculoAcumulado : Number = null;
    osso : Number = null;
}
