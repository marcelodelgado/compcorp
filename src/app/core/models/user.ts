export class User {
    nome : string = "";
    objetivo : string = "";
    perdaSemana : Number = null;
    pesoFinal : Number = null;
    pesoInicial : Number = null;
    porcGorduraInicial: Number = null;
}
