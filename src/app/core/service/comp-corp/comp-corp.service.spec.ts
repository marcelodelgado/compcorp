import { TestBed } from '@angular/core/testing';

import { CompCorpService } from './comp-corp.service';

describe('CompCorpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CompCorpService = TestBed.get(CompCorpService);
    expect(service).toBeTruthy();
  });
});
