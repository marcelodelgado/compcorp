import { Injectable } from '@angular/core';
import { CompCorp } from "../../models/comp-corp";
import { AngularFireDatabase } from "@angular/fire/database";
import { map } from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class CompCorpService {

  constructor(private db: AngularFireDatabase) { }

  /* Composição corporal */
  insert(compCorp : CompCorp){
    this.db.list(`compCorp`).push(compCorp).
    then((result:any) => {
      console.log(result.key);
    })
  }

  /*update(user : User, key : string){
    this.db.list('user').update(key, user)
      .catch((error: any) => {     
    } );  
  }*/

  getAll(userKey : string){
    //TODO ORDENAR O GETALL POR DATAS
    return this.db.list('compCorp', ref => ref.orderByChild('userKey').equalTo(userKey))
      .snapshotChanges()
      .pipe(
        map(changes => {
          return changes.map(u => ({ key: u.payload.key, ...u.payload.val() })); 
        })
      );
  }

  /*delete(key : string){
    this.db.object(`user/${key}`).remove();
  }*/
  
}
