import { Injectable } from '@angular/core';
import { User } from "../../../core/models/user";
import { AngularFireDatabase } from "@angular/fire/database";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})

export class UserService {

  constructor(private db:AngularFireDatabase) {  }

  /* Dados do usuário e metas */
  insert(user : User){
    this.db.list('user').push(user)
      .then((result: any) => {
        console.log(result.key);
      });
  }

  update(user : User, key : string){
    this.db.list('user').update(key, user)
      .catch((error: any) => {
        console.error(error);
    } );  
  }

  getAll(){
    return this.db.list('user')
      .snapshotChanges()
      .pipe(
        map(changes => {
          return changes.map(u => ({ key: u.payload.key, ...u.payload.val() }));
        })
      );
  }

  delete(key : string){
    this.db.object(`user/${key}`).remove();
  }
  
}
