import { Component, OnInit } from '@angular/core';
import { User } from '../../../core/models/user';
import { UserService } from '../../../core/service/user/user.service';
import { FormBuilder, Validators } from '@angular/forms';
import { LoggedUserDataService } from "src/app/shared/service/logged-user-data/logged-user-data.service";

@Component({
  selector: 'app-config-form',
  templateUrl: './config-form.component.html',
  styleUrls: ['./config-form.component.css']
})

//TODO CRIAR VALIDAÇÃO ENTRE OBJETIVO E SINAL DA META DA SEMANA
//TODO CRIAR VALIDAÇÃO ENTRE OBJETIVO E RELAÇÃO ENTRE PESO INICIAL E FINAL
//TODO CRIAR POSSIBILIDADE DE O USUÁRIO COLOCAR % DE GORDURA COMO OBJETIVO

export class ConfigFormComponent implements OnInit {
  user : User;
  key : string = ""; 
  userForm = this.initializeUserForm(); 

  constructor(private userService : UserService, private loggedUserDataService : LoggedUserDataService, private fb: FormBuilder) { }
  

  ngOnInit() {
    this.loggedUserDataService.currentUser.subscribe(data => {
      if (data.user && data.key) {
        this.user = new User();
        this.user.nome = data.user.nome;
        this.user.objetivo = data.user.objetivo;
        this.user.perdaSemana = data.user.perdaSemana;
        this.user.pesoInicial = data.user.pesoInicial;
        this.user.pesoFinal = data.user.pesoFinal;
        this.user.porcGorduraInicial = data.user.porcGorduraInicial;
        this.key = data.key;
        this.userForm.setValue(this.user);
      }
    })

    //this.userForm.valueChanges.subscribe(console.log);
  }

  initializeUserForm ()
  {
    return this.fb.group({
      nome :  ["", [Validators.required]],
      objetivo : ["", [Validators.required]],
      perdaSemana : ["", [Validators.required, Validators.min(-1), Validators.max(1)]],
      pesoInicial : ["", [Validators.required, Validators.min(30), Validators.max(250)]],
      pesoFinal : ["", [Validators.required, Validators.min(30), Validators.max(250)]],
      porcGorduraInicial : ["", [Validators.required, Validators.min(3), Validators.max(60)]]
    });
  }

  get nome(){
    return this.userForm.get('nome');
  }

  get objetivo(){    
    return this.userForm.get('objetivo');    
  }

  get perdaSemana(){
    return this.userForm.get('perdaSemana');
  }

  get pesoInicial(){
    return this.userForm.get('pesoInicial');
  }
  
  get pesoFinal(){
    return this.userForm.get('pesoFinal');
  }

  get porcGorduraInicial(){
    return this.userForm.get('porcGorduraInicial');
  }

  onSubmit() {
    if (this.key) {
      this.userService.update(this.userForm.value, this.key);
      this.key = "";
    }
    else {
      this.userService.insert(this.userForm.value);
    }

    this.userForm = this.initializeUserForm();
  }

}