import { Component, OnInit } from '@angular/core';
import { User } from '../../../core/models/user';
import { UserService } from '../../../core/service/user/user.service';
import { LoggedUserDataService } from '../../../shared/service/logged-user-data/logged-user-data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-config-user-list',
  templateUrl: './config-user-list.component.html',
  styleUrls: ['./config-user-list.component.css']
})
export class ConfigUserListComponent implements OnInit {
  users : Observable<any>;

  constructor(private userService : UserService, private loggedUserDataService : LoggedUserDataService) { }

  ngOnInit() {
    this.users = this.userService.getAll();
  }

  delete( key : string ) {
    this.userService.delete(key);
    //TODO DELETE TB DEVE APAGAR AS ENTRADAS NO COMPCORP PARA O USUÁRIO QUE ESTA SENDO DELETADO.
  }

  edit( user : User, key: string ) {
    this.loggedUserDataService.changeUser(user, key);    
  }

}