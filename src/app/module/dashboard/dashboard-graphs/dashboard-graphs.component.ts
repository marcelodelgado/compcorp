import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../../core/models/user';
import { CompCorp } from '../../../core/models/comp-corp';
import { CompCorpService } from '../../../core/service/comp-corp/comp-corp.service';
import { LoggedCompCorpService } from '../../../shared/service/logged-comp-corp/logged-comp-corp.service';
import { LoggedUserDataService } from '../../../shared/service/logged-user-data/logged-user-data.service';
import { Chart } from 'chart.js';
import 'regression';

@Component({
  selector: 'app-dashboard-graphs',
  templateUrl: './dashboard-graphs.component.html',
  styleUrls: ['./dashboard-graphs.component.css']
})

//TODO USAR O SERVIÇO DE COMPCORP COMPARTILHADO PORÉM TOMAR CUIDADO QUANDO ESSA ABA FOR CLICADA PRIMEIRO (COMO TRATAR? DENTRO DO PRÓPRIO SERVIÇO?).

export class DashboardGraphsComponent implements OnInit {
  userKey : string = "";
  user : User;
  //ctx : HTMLElement = document.getElementById("myChart");
  graficoGordMusc : any = [];
  graficoPesoAcumulado : any = [];
  graficoGorduraAcumulado : any = [];
  graficoMusculoAcumulado : any = [];

  compCorps : Observable<any>;
  compCorpsArray : Array<any>;    

  labels = [];
  dataGord = [];
  dataMusc = [];
  dataGordAcum = [];
  dataMuscAcum = [];
  dataPesoAcum = [];
  

  constructor(private compCorpService: CompCorpService, private loggedUserDataService : LoggedUserDataService) { }

  ngOnInit() {
    this.loggedUserDataService.currentUser.subscribe(data => {
      if (data.user && data.key) {        
        this.userKey = data.key;
        this.user = data.user;
        this.compCorps = this.compCorpService.getAll(this.userKey);
        this.compCorps.subscribe(dataComp => {
            this.compCorpsArray = dataComp;
            
            this.calculateDerivedData();
            this.separaSeriesDados(this.compCorpsArray);

            this.criarGraficoGordMusc(this.compCorpsArray);
            this.criarGraficoPesoAcumulado(this.compCorpsArray);
            this.criarGraficoGorduraAcumulado(this.compCorpsArray);
            this.criarGraficoMusculoAcumulado(this.compCorpsArray);            
        });
      }      
    });
  }

  separaSeriesDados (compArray) {
    compArray.forEach(comp => {
      this.labels.push(comp.data);
      this.dataGord.push(comp.gorduraKg);
      this.dataMusc.push(comp.musculoKg);
      this.dataGordAcum.push(comp.gorduraAcumulada);
      this.dataMuscAcum.push(comp.musculoAcumulado);
      this.dataPesoAcum.push(comp.pesoAcumulado);
    });
  }

  criarGraficoGordMusc(compArray){
    this.graficoGordMusc = new Chart('canvasGordMusc', {
      type: 'line',
      data: {
        labels: this.labels,
        datasets: [
          {
            label: 'Gordura(kg)',
            data: this.dataGord,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)'                  
            ],
            borderWidth: 1
          },
          {
            label: 'Musculo(kg)',
            data: this.dataMusc,
            backgroundColor: [
                'rgba(66, 134, 244, 0.4)'
            ],
            borderColor: [
                'rgba(66, 134, 244,1)'                  
            ],
            borderWidth: 1
          }
        ]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        }
      }
    });
  }

  criarGraficoGorduraAcumulado(compArray){
    this.graficoGorduraAcumulado = new Chart('canvasGorduraAcu', {
      type: 'line',
      data: {
          labels: this.labels,
          datasets: [{
              label: 'Gordura Acumulado(kg)',
              data: this.dataGordAcum,
              backgroundColor: [
                  'rgba(255, 99, 132, 0.4)'
              ],
              borderColor: [
                  'rgba(255,99,132,1)'
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
    });
  }

  criarGraficoMusculoAcumulado(compArray){
    this.graficoMusculoAcumulado = new Chart('canvasMusculoAcu', {
      type: 'line',
      data: {
          labels: this.labels,
          datasets: [{
              label: 'Musculo Acumulado(kg)',
              data: this.dataMuscAcum,
              backgroundColor: [
                  'rgba(66, 134, 244, 0.4)'
              ],
              borderColor: [
                  'rgba(66, 134, 244, 1)'
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
    });    
  }

  criarGraficoPesoAcumulado(compArray){
    this.graficoPesoAcumulado = new Chart('canvasPesoAcu', {
      type: 'line',
      data: {
          labels: this.labels,
          datasets: [{
              label: 'Peso Acumulado(kg)',
              data: this.dataPesoAcum,
              backgroundColor: [
                  'rgba(249, 180, 29, 0.4)'
              ],
              borderColor: [
                  'rgba(249, 180, 29, 1)'
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
    });
  }
  

  calculateDerivedData(){
    var compCorpAnterior : CompCorp = null;
    this.compCorpsArray.forEach((compCorp : CompCorp, i) => {
      this.createDateObject(compCorp);
      if (i != 0)
      {
        compCorpAnterior = this.compCorpsArray[i -1]
      }
      this.compCorpsArray[i] = this.calculaDeltaAcumulado(compCorp, compCorpAnterior);      
    })
  }

  createDateObject(comp : CompCorp){
    var dateSplit = {};
    dateSplit = comp.data.split("/");

    comp.dataObjeto = new Date(dateSplit[2], dateSplit[1] - 1, dateSplit[0]);    
  }

  calculaDeltaAcumulado(comp, compAnt){
    if (compAnt == null)
    {
      comp.peso = comp.peso;
      comp.gordura = comp.gordura;
      comp.musculo = comp.musculo;
      comp.hidratacao = comp.hidratacao;
      comp.osso = comp.osso;

      comp.pesoDelta = 0;
      comp.pesoAcumulado = 0;
      comp.pesoEstimado = comp.peso;

      comp.gorduraKg = comp.peso * comp.gordura / 100;
      comp.gorduraEstimada = comp.gordura;
      comp.gorduraDelta = 0;
      comp.gorduraAcumulada = 0;
      
      comp.massaMagraKg = comp.peso - comp.gorduraKg;
      comp.massaMagraDelta = 0;
      comp.massaMagraAcumulada = 0;
      
      comp.musculoKg = comp.peso * comp.musculo / 100;
      comp.musculoEstimado = comp.musculoKg;
      comp.musculoDelta = 0;
      comp.musculoAcumulado = 0;
    }
    else{
      comp.peso = comp.peso;
      comp.gordura = comp.gordura;
      comp.musculo = comp.musculo;
      comp.hidratacao = comp.hidratacao;
      comp.osso = comp.osso;

      comp.pesoDelta = comp.peso - compAnt.peso;
      comp.pesoAcumulado = compAnt.pesoAcumulado + comp.pesoDelta;
      //comp.pesoEstimado = ;

      comp.gorduraKg = comp.peso * comp.gordura / 100;
      comp.gorduraDelta = comp.gorduraKg - compAnt.gorduraKg;
      comp.gorduraAcumulada = compAnt.gorduraAcumulada + comp.gorduraDelta;
      //comp.gorduraEstimada = comp.gordura;
      
      comp.massaMagraKg = comp.peso - comp.gorduraKg;
      comp.massaMagraDelta = comp.massaMagraKg - compAnt.massaMagraKg;
      comp.massaMagraAcumulada = compAnt.massaMagraAcumulada + comp.massaMagraDelta;
      
      comp.musculoKg = comp.peso * comp.musculo / 100;      
      comp.musculoDelta = comp.musculoKg - compAnt.musculoKg;
      comp.musculoAcumulado = compAnt.musculoAcumulado + comp.musculoDelta;
      //comp.musculoEstimado = comp.musculoKg;
    }
    return comp;
  }

  srn(stringNum)
  {    
    if (stringNum !== "" && typeof(stringNum) === 'string')
    {
      var string = stringNum.replace(",", ".");      
      return Number(string);
    }
  }

}
