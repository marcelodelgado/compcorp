import { Component, OnInit } from '@angular/core';
import { LoggedUserDataService } from '../../../shared/service/logged-user-data/logged-user-data.service';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.css']
})
export class DashboardPageComponent implements OnInit {
  userName = "";

  constructor(private loggedUserDataService : LoggedUserDataService) { }

  ngOnInit() {
    if (this.userName != this.loggedUserDataService.selectedUserName)
    {
      this.userName = this.loggedUserDataService.selectedUserName;
    }

  }

}
