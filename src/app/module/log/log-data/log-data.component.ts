import { Component, OnInit } from '@angular/core';
import { Observable, ObjectUnsubscribedError } from 'rxjs';
import { CompCorpService } from '../../../core/service/comp-corp/comp-corp.service';
import { CompCorp } from "../../../core/models/comp-corp"
import { LoggedCompCorpService } from '../../../shared/service/logged-comp-corp/logged-comp-corp.service';
import { LoggedUserDataService } from '../../../shared/service/logged-user-data/logged-user-data.service';

@Component({
  selector: 'app-log-data',
  templateUrl: './log-data.component.html',
  styleUrls: ['./log-data.component.css']
})

//TODO AQUI VAI SER FEITA CONFERENCIA SE OS CALCULOS FORAM FEITOS PARA CADA ENTRADA RECUPERADA, SE NÃO FAZER OS CALCULOS E SALVAR NO BANCO.

export class LogDataComponent implements OnInit {
  userKey : string = "";
  compCorps : Observable<any>;
  compCorpsList : [CompCorp];

  constructor(private compCorpService: CompCorpService, private loggedUserDataService : LoggedUserDataService, private loggedCompCorpService : LoggedCompCorpService) { }

  ngOnInit() {
    if (this.loggedUserDataService.selectedUserKey != this.userKey)
    {      
      this.userKey = this.loggedUserDataService.selectedUserKey;
      this.compCorps = this.compCorpService.getAll(this.userKey);

      var compCorpSub = this.compCorps.subscribe(dataComp => {
        if (dataComp.length != 0)
        { 
          dataComp.sort(this.sortObjectData);         
          this.compCorpsList = dataComp;
          console.log(this.compCorpsList);

          this.loggedCompCorpService.changeCompCorp(this.compCorpsList, this.userKey);          
          compCorpSub.unsubscribe();
        }
      });
    }
  }

  sortObjectData (a, b){
    console.log("lexicographical comparison");
    console.log(a.data.localeCompare(b.data))
    return a.data.localeCompare(b.data);
  }

  

}
