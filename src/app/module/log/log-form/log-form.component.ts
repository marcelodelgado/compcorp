import { Component, OnInit } from '@angular/core';
import { User } from '../../../core/models/user';
import { CompCorp } from '../../../core/models/comp-corp'; 

import { CompCorpService } from '../../../core/service/comp-corp/comp-corp.service';
import { UserService } from '../../../core/service/user/user.service';
import { LoggedUserDataService } from '../../../shared/service/logged-user-data/logged-user-data.service';

import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-log-form',
  templateUrl: './log-form.component.html',
  styleUrls: ['./log-form.component.css']
})

//TODO FAZER A VALIDAÇÃO DO FORMULÁRIO
//TODO ADAPTAR AS DATAS PARA SEREM GRAVADAS NO BANCO COMO NUMERO
//TODO ADAPTAR TODAS AS CONTAS PARA LIDAR COM AS NOVAS DATAS
//TODO EDITAR E REMOVER COMPCORPS
//TODO COLOCAR COMPCORPS EM UMA TABELA

export class LogFormComponent implements OnInit {

  user : User = {nome : "", perdaSemana : null, pesoInicial : null, pesoFinal : null, porcGorduraInicial : null, objetivo : "" };
  compCorp : CompCorp;
  key : string = ""; 
  //compCorpForm : FormGroup;
  compCorpForm = this.initializeCompCorpForm();


  constructor(private compCorpService: CompCorpService, private userService : UserService, private loggedUserDataService : LoggedUserDataService, private fb: FormBuilder) { }

  ngOnInit() {
    if (this.loggedUserDataService.selectedUserKey != this.key)
    {
      this.key = this.loggedUserDataService.selectedUserKey;
      this.user = new User();
      this.loggedUserDataService.currentUser.subscribe(data => {
        if (data.user && data.key) {
          this.user = new User();
          this.user.nome = data.user.nome;
          this.user.perdaSemana = data.user.perdaSemana;
          this.user.pesoInicial = data.user.pesoInicial;
          this.user.pesoFinal = data.user.pesoFinal;
          this.user.porcGorduraInicial = data.user.porcGorduraInicial;
          this.key = data.key;

          this.compCorpForm = this.initializeCompCorpForm(); 
        }
      });
    }    
    this.compCorp = new CompCorp();
  }

  initializeCompCorpForm ()
  {
    return this.fb.group({      
      userKey : [this.key],
      data : ["", [Validators.required]],
      peso : ["", [Validators.required, Validators.min(30), Validators.max(250)]],
      gordura : ["", [Validators.required, Validators.min(0), Validators.max(70)]],
      hidratacao : ["", [Validators.min(10), Validators.max(70)]],
      musculo : ["", [Validators.min(10), Validators.max(70)]],
      osso : ["", [Validators.min(1), Validators.max(30)]]
    });
  }

  get data(){
    return this.compCorpForm.get('data');
  }

  get peso(){
    return this.compCorpForm.get('peso');
  }

  get gordura(){
    return this.compCorpForm.get('gordura');
  }

  get hidratacao(){
    return this.compCorpForm.get('hidratacao');
  }

  get musculo(){
    return this.compCorpForm.get('musculo');
  }
  
  get osso(){
    return this.compCorpForm.get('osso');
  }

  checkButtonDisabled()
  {
    console.log(this.compCorpForm.valid && this.key != "");
    if (this.compCorpForm.valid && this.key != "")
      return false;
    else
      return true;
  }

  onSubmit() {
    if (this.key) { 
      console.log(this.compCorpForm.value);
      //this.compCorp.userKey = this.key;    
      this.compCorpService.insert(this.compCorpForm.value);
    }
    //this.compCorp = new CompCorp();
    this.compCorpForm = this.initializeCompCorpForm();
  }

}
