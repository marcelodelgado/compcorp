import { TestBed } from '@angular/core/testing';

import { LoggedCompCorpService } from './logged-comp-corp.service';

describe('LoggedCompCorpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoggedCompCorpService = TestBed.get(LoggedCompCorpService);
    expect(service).toBeTruthy();
  });
});
