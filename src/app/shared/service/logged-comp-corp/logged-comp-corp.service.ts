import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CompCorp } from '../../../core/models/comp-corp';

@Injectable({
  providedIn: 'root'
})
export class LoggedCompCorpService {
  private compCorpList = new BehaviorSubject({ compCorp: null, key: '' });  
  currentCompCorp = this.compCorpList.asObservable();
  selectedUserKey = "";

  constructor() { }

  changeCompCorp(compCorp: [CompCorp], key: string)
  {
    this.selectedUserKey = key;
    this.compCorpList.next({ compCorp: CompCorp, key: key});    
  }
}
