import { TestBed } from '@angular/core/testing';

import { LoggedUserDataService } from './logged-user-data.service';

describe('LoggedUserDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoggedUserDataService = TestBed.get(LoggedUserDataService);
    expect(service).toBeTruthy();
  });
});
