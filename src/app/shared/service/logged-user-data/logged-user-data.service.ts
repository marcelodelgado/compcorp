import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../../../core/models/user';

@Injectable({
  providedIn: 'root'
})
export class LoggedUserDataService  {
  private userSource = new BehaviorSubject({ user: null, key: '' });  
  currentUser = this.userSource.asObservable();  
  selectedUserKey = "";
  selectedUserName = "";
  constructor() { }

  changeUser(user: User, key: string)
  {
    this.selectedUserKey = key;
    this.selectedUserName = user.nome;
    this.userSource.next({ user: user, key: key});    
  }


}