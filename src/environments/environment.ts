// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDTZ8ga-z5uk9Hw9E6pdNg9fdJK-yL1ykI",
    authDomain: "compcorp.firebaseapp.com",
    databaseURL: "https://compcorp.firebaseio.com",
    projectId: "compcorp",
    storageBucket: "compcorp.appspot.com",
    messagingSenderId: "256266756510"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
